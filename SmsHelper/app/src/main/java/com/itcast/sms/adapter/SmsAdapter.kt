package com.itcast.sms.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.itcast.sms.bean.Sms


/**
 * ClassName:SmsAdapter
 * Description:短信列表适配器
 */
class SmsAdapter(var list:List<Sms>):BaseAdapter() {
    //条目布局
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val textView = TextView(parent.context)
        textView.text = list[position].neirong
        textView.setPadding(10,10,10,10)
        return textView
    }
    //条目数据
    override fun getItem(position: Int): Any {
        return list[position]
    }
    //条目id
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    //条目个数
    override fun getCount(): Int {
        return list.size
    }
}