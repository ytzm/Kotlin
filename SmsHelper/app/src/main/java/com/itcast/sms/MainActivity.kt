package com.itcast.sms

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.ListView
import com.google.gson.Gson
import com.itcast.sms.adapter.SmsAdapter
import com.itcast.sms.bean.Sms
import com.itcast.sms.bean.SmsResult
import org.jetbrains.anko.*
import java.net.URL

/**
 * 主界面
 */
class MainActivity : AppCompatActivity() {
    val list = ArrayList<Sms>()
    lateinit var listView: ListView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //布局文件布局
//        setContentView(R.layout.activity_main)
        verticalLayout {
            //头部
            textView("短信助手") {
                backgroundResource = R.drawable.navigationbarback
                //文字居中
                gravity = Gravity.CENTER
                //文字颜色
                textColor = Color.WHITE
            }
            //短信列表
            listView = listView {
                //条目点击事件
                setOnItemClickListener { parent, view, position, id ->
                    //                   Toast.makeText(this@MainActivity,"点击了${}",Toast.LENGTH_LONG).show()
                    //anko库toast
//                   toast("点击了${list[position].neirong}")
                    //跳转到SmsActivity  并且将数据传递过去
                    val sms = list[position].neirong
                   startActivity<SmsActivity>("sms" to sms)
                }
//                adapter =
            }
        }

        //加载网络数据
        loadSms()
    }

    private fun loadSms() {
        //网络请求必须开启新线程
        doAsync {
            val path = "http://api.avatardata.cn/ZhuFuYu/QueryZhuFu?key=1b2bd09855df44cc9e92b54395a46e77&typename=爱情&page=1&rows=20"
            //网络请求直接获取网络返回值
            val result = URL(path).readText()
            //将返回值转换为SmsResult  Gson
            val gson = Gson()
            val smsResult = gson.fromJson(result, SmsResult::class.java)
            //短信集合
            list.clear()
            list.addAll(smsResult.result)

            //适配 主线程中适配
            uiThread {
                val adapter = SmsAdapter(list)
                //设置适配器
                listView.adapter = adapter
            }
        }
    }
}
