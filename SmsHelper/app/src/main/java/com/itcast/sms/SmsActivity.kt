package com.itcast.sms

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.EditText
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick


/**
 * ClassName:SmsActivity
 * Description:短信编辑界面
 */
class SmsActivity:AppCompatActivity() {
    lateinit var et_phone:EditText
    lateinit var et_msg:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //获取传递的数据
        val sms = intent.getStringExtra("sms")
        verticalLayout {
            //上部
            relativeLayout {
                backgroundResource = R.drawable.navigationbarback
                padding = 20
                imageView(imageResource = R.drawable.back_selector){
                    onClick { finish() }
                }

                textView("新短信").lparams{
                    centerInParent()
                }
            }
            //电话号码输入
            linearLayout {
                gravity = Gravity.CENTER_VERTICAL
                textView("收件人:")
                //联系人输入框
                et_phone = editText {
                    hint = "请输入收件人"
                }.lparams(width = 0, weight = 1f)
//                imageView(imageResource = R.drawable.add_selector)
            }
            //短信列表
            listView {

            }.lparams(width = matchParent, height = 0, weight = 1f)
            //短信编辑和发送按钮
            linearLayout {
                gravity = Gravity.CENTER_VERTICAL
                //短信输入框   将获取的短信添加进去
                et_msg = editText(sms).lparams(width = 0, weight = 1f)
                //发送按钮
                button("发送") {
                    onClick { sendMsg() }
                }
            }
        }

    }

    /**
     * 发送短信
     */
    private fun sendMsg() {
        //获取联系人
        val phone = et_phone.text.toString()
        //获取短信内容
        val msg = et_msg.text.toString()

        //调用android的系统api发送短信
        //调用系统短信发送服务发送
        val smsManager = android.telephony.SmsManager.getDefault()
        //拆分短信内容（手机短信长度限制）
        val divideContents = smsManager.divideMessage(msg)
        for (text in divideContents) {
            smsManager.sendTextMessage(phone, "123345554", text, null, null)
        }
    }
}