package com.itcast.sms.bean


//每一条短信bean
data class Sms(var type_name:String,var neirong:String,var level:Int)

data class SmsResult(var total:Int,var result:List<Sms>,var error_code:Int,var reason:String)