package com.itcast.sms

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListener
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import org.jetbrains.anko.imageView
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.verticalLayout


/**
 * ClassName:SplashActivity
 * Description:欢迎界面
 */
class SplashActivity:AppCompatActivity(), ViewPropertyAnimatorListener {
    /**
     * 动画结束
     */
    override fun onAnimationEnd(view: View?) {
//        val intent = Intent(this,MainActivity::class.java)
//        startActivity(intent)
        //使用anko库跳转
        startActivity<MainActivity>()
        //取消欢迎界面
        finish()
    }

    /**
     * 动画取消
     */
    override fun onAnimationCancel(view: View?) {
    }

    /**
     * 动画开始
     */
    override fun onAnimationStart(view: View?) {
    }

    //lateinit 延迟初始化
    lateinit var imageView:ImageView
    //选择一个参数的onCreate方法
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //通过anko 布局 dsl
        verticalLayout {//竖直方向线性布局
            //背景图片
            imageView = imageView(R.drawable.splash){
                scaleType = ImageView.ScaleType.CENTER_CROP
                //设置图片透明度
                alpha = 0f
            }
        }

        //处理图片动画
        ViewCompat.animate(imageView).alpha(1f).setDuration(300).setListener(this)
    }
}