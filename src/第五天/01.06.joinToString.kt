package 第五天
/**
 * 讲集合转换为json字符串
 * Gson
 */
fun main(args: Array<String>) {
    val list = listOf("张三","李四","王五")
    println(list)
    //按照(张三|李四|王五)打印
    val result = list.joinToString()
    println(result)
    val result1 = list.joinToString(separator = "|",prefix = "(",postfix = ")")
    println(result1)
}