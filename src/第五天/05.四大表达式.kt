package 第五天
fun main(args: Array<String>) {
    val list:MutableList<String> = mutableListOf("张三","李四","王五")
    list.add("赵六")
    //高阶函数  lambda表达式  扩展函数
    /*---------------------------- apply ----------------------------*/
    /**
     * 任意类型都有扩展函数apply
     * apply函数参数函数作用域在当前对象内部
     * 可以直接调用对象里面的函数
     * apply函数返回值就是对象本身
     */
    list.apply {
        this.add("赵六")
        add("周七")
    }.add("找嘎")

    /*---------------------------- let ----------------------------*/
    list.let {

    }
    /*---------------------------- with ----------------------------*/
    with(list){

    }
    /*---------------------------- run ----------------------------*/
    list.run {

    }
}

class Stu{
    fun apply(){
        this.sayHello()
        sayHello()
    }
    fun sayHello(){
        this
    }
}

