package 第五天
/**
 * 函数作为参数或者函数和作为返回值就是高阶函数
 */
fun main(args: Array<String>) {
    var a = 10
    var b = 20
    var sum = 0
    var result = 0
//    sum = add(a, b)
//    result = sub(a, b)

    //传递函数引用 ::add
    sum = cacl(a,b,::add)
    result = cacl(a,b,::sub)

    println(sum)
    println(result)
}
//第三个参数block是函数类型  (Int,Int)->Int
fun cacl(a:Int,b:Int,block:(Int,Int)->Int):Int{
    return block(a,b)
}


//a+b
fun add(a:Int,b:Int) = a+b
//a-b
fun sub(a:Int,b:Int) = a-b



