package 第五天
/**
 * map集合每一个元素就是Entry对象 Entry对象里面有key value
 */
fun main(args: Array<String>) {
    val map = mapOf("中国" to "China","英国" to "England")

    //通过entry
//    for (entry in map) {
//        val key = entry.key
//        val value = entry.value
//        println("key=$key value=$value")
//    }
    //直接获取每一个key和value
//    for ((key,value) in map){
//        println("key=$key value=$value")
//    }
    //单独获取key
//    val keys = map.keys
//    for (key in keys) {
//        println(key)
//    }

    //单独获取value
//    val values = map.values
//    for (value in values) {
//        println(value)
//    }

    //通过迭代器Iterator迭代
    val iteractor = map.iterator()
    while (iteractor.hasNext()){
        val next = iteractor.next()
        val key = next.key
        val value = next.value
        println("key=$key value=$value")
    }
}