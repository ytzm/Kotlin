package 第五天
/**
 * 如果lambda表达式只有一个参数 可以不写  用it表示
 */
fun main(args: Array<String>) {
    var a = 10
    add(10){
        println(it)
    }

//    val list = listOf<String>()
//    list.forEach {
//        println(it)
//    }
}
fun add(a:Int,action:(Int)->Unit){
    action(a+10)
}