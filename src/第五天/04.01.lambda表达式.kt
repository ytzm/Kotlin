package 第五天
/**
 * lambda表达式就是匿名函数
 */

fun main(args: Array<String>) {
    var a = 10
    var b = 20
    var sum = 0
    var result = 0
//    sum = add(a, b)
//    result = sub(a, b)

    //传递函数引用 ::add
//    sum = cacl(a,b,::add)
//    result = cacl(a,b,::sub)

    //lambda表达式
    sum = cacl(a,b,{a,b->
        a+b
    })
    result = cacl(a,b,{a,b->
        a-b
    })

    println(sum)
    println(result)
}


