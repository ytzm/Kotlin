package 第五天

fun main(args: Array<String>) {
    val  list = listOf("n","f","q","z")
    //正序排序
    println(list.sorted())
    //倒叙排序
    println(list.sortedDescending())


    //对象排序
    val humanList = listOf(Human("张三",20),Human("李四",30),Human("王五",10))
    //对人按照年纪进行正序排序
    println(humanList.sortedBy { it.age })
    //根据年纪字段进行倒叙排序
    println(humanList.sortedByDescending { it.age })

    //自定义比较器比较(传递比较规则进行比较)
    //kotlin匿名对象 需要加上object
    println(humanList.sortedWith(object : Comparator<Human> {
        override fun compare(o1: Human, o2: Human): Int {
            val o1Age = o1.age
            val o2Age = o2.age
            return o2Age-o1Age
        }
    }))
}
//class MyComparetor:Comparator<Human>{
//    override fun compare(o1: Human?, o2: Human?): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//}
class Human(var name:String,var age:Int){
    override fun toString(): String {
        return "Human(name='$name', age=$age)"
    }
}

