package 第五天

fun main(args: Array<String>) {
    val list = listOf("张三","李四","王五","赵六","张四","李国华","王二麻子")
    //把姓张的分成一组  姓李的分成一组  其他人一组

    //java处理
//    val zhsangLlist = mutableListOf<String>()
//    val liLlist = mutableListOf<String>()
//    list.forEach {
//        if(it.startsWith("张")){
//            zhsangLlist.add(it)
//        }else if(it.startsWith("李")){
//            liLlist.add(it)
//        }
//    }
//    map.put("张",zhanglist)
//    map.put("李",zhanglist)

    //kotlin处理
    val map = list.groupBy {
        when{
            it.startsWith("张")->"姓张的"
            it.startsWith("李")->"李"
            else->"其他人"
        }
    }
    println(map)
}