package 第五天
//go  swift js python
//java函数没有状态
//kotlin函数是有状态  闭包让函数有了状态
//kotlin里面闭包一般都是值得lambda表达式
fun main(args: Array<String>) {
//    printA()
//    printA()
//    printA()
    //返回的是函数
    val result = close()
    //调用result函数
    result()
    result()
    result()
}
fun printA(){
    var a = 10
    println(a)
    a++
}
//函数返回了一个没有参数没有返回值的函数
fun close():()->Unit{
    var a = 10
    return {
        println(a++)
    }
}



