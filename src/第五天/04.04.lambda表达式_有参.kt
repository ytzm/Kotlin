package 第五天
fun main(args: Array<String>) {
    //有参的lambda表达式 不执行
//    {a:Int,b:Int->
//        a+b
//    }
    //执行lambda表达式
    //3+5
    val sum = {a:Int,b:Int->
        a+b
    }(3,5)
    println(sum)
}