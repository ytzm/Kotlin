package 第五天

fun main(args: Array<String>) {
    val list = listOf("张三", "李四", "王五")
    //for
//    for (s in list) {
//        println(s)
//    }
//    for ((index,s) in list.withIndex()) {
//        println("index=$index s=$s")
//    }
    //foreach
//    list.forEach {
//        println(it)
//    }
    list.forEachIndexed { index, s ->
        println("index=$index s=$s")
    }
}