package 第五天
import java.util.*

/**
 * list
 * set
 * map
 *
 * Any相当于java的Object
 */
fun main(args: Array<String>) {
    //定义集合保存张三  李四 王五
    //1.通过listOf创建集合  只读集合  不能修改 不能添加
    val list = listOf("张三","李四","王五")
    //访问集合
    println(list.get(0))//获取角标为0的数据
    println(list[0])//get
    //修改集合
//    list[0] =  "赵六"
    //2.mutableListOf创建可修改的集合  可读可以修改可以添加
    val mutableList = mutableListOf("张三","李四","王五")
    mutableList.get(0)
    mutableList[0]
    //修改集合
    mutableList[0] = "赵六"
    //添加新数据
    mutableList.add("王二麻子")
    //打印集合
    println(mutableList)

    //3.ArrayLlist  linkList  Vector
    val arrayList = arrayListOf("张三","李四","王五")
    arrayList.add("哈哈")

    val linkList = LinkedList<String>()

    val vector = Vector<String>()


}