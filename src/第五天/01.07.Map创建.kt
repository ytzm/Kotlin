package 第五天
/**
 * map相当于是词典 有键值对
 * map的key不能重复  重复会覆盖原来的数据
 * 也可以创建java的map
 */
fun main(args: Array<String>) {
    //通过mapOf创建map集合 只读的map集合
    val map = mapOf("中国" to "China","美国" to "America")
    //通过key值获取value值
//    println(map.get("中国"))

    //创建一个可读可写的集合
    val muMap = mutableMapOf("中国" to "China","美国" to "America")
    println(muMap.get("中国"))
    println(muMap["中国"])
    //添加新元素
    muMap.put("英国","EG")
    muMap.put("中国","哈哈")

    println(muMap)

    //创建java 的map
    val hashMap = hashMapOf("中国" to "China","美国" to "America")
    val linkedHashMap = linkedMapOf("中国" to "China","美国" to "America")

}