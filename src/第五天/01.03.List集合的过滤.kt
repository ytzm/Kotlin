package 第五天

fun main(args: Array<String>) {
    val list = listOf("张三","李四","王五","赵六","张四","李五","李六")
    val list2 = listOf("张五","啊哈","张六")
    //把所有的姓张的过滤出来
//    val mulist = mutableListOf<String>()
//    list.forEach {
//        if(it.startsWith("张")){
//            mulist.add(it)
//        }
//    }
    val resultList = list.filter { it.startsWith("张") }
//    println(resultList)

    //将两个集合中所有姓张的添加到同一个集合中
    val mulist = mutableListOf<String>()
    list.filterTo(mulist){ it.startsWith("张") }
//    println(mulist)
    list2.filterTo(mulist){ it.startsWith("张") }
//    println(mulist)

    //将所有角标为偶数的元素过滤出来
    //index元素角标 s元素
    val newList = list.filterIndexed { index, s -> index%2==0 }
    println(newList)
}
