package 第四天

import java.util.ArrayList

class A(){//可以不写(),但不写下面的就是主构函数了
    constructor(name: String,age: Int):this()
    var name: String = "点点滴滴"//不赋初值会报错,不存在默认值问题
    var age: Int = 1
}

fun main(args: Array<String>) {
    var a = A()
    var c = Father()
    println(a.name)

//    var b = Son()

    val map = hashMapOf(1 to "one",2 to "two",3 to "three")
    for (aa in map){

    }

    val list = arrayListOf<String>("a","b")
    val list2 = mutableListOf(1,"b","c")
    
}

open class Father(name: String = "sss",age: Int = 1)

class Son:Father{
    constructor(name: String):super()//父类构造函数有默认值,子类就可以不传
    constructor(name: String,age: Int,phone: Int):super(name,age)
}

