/**
 * 嵌套类  在类里面定义的class
 * 嵌套类不依赖外部类,不能够访问外部类的字段或者函数
 */
fun main(args: Array<String>) {
    val inClass = OutClass.InClass()
}

class OutClass {
    var name = "张三"
    //静态类
    class InClass {
        //嵌套类
        fun sayHello() {
//            println(name)
        }
    }
}