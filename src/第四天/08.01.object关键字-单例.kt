/**
 * 单例:对象在内存中只有一个实例
 * 用object代替class
 * 可以直接使用对象名.字段或者函数调用
 */
fun main(args: Array<String>) {
    println(Data.name)
    println(Data.age)
    Data.sayHello()
}
//单例
//只需要特定的字段为static  其他字段都需要创建对象才能访问
object Data{
    var name = "张三"
    var age = 10
    var phone = 10
    fun sayHello(){
        println("hello $name")
    }
}