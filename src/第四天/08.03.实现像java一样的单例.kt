fun main(args: Array<String>) {
    val single = Single.single
    println(single.name)
    single.sayHello()
}

//1.私有构造函数
//2.对象引用
//3.提供静态方法获取对象引用属性
class Single private constructor() {
    var name = "张三"
    //私有构造函数
    companion object {
        val single by lazy { Single() }
    }
    fun sayHello(){
        println("hello")
    }
}
