/**
 * 数据类就是只保存数据,里面没有任何业务逻辑的类 对应的就是java的bean类
 */
fun main(args: Array<String>) {
    val sms = Sms(100,"哈哈","15434567890")
    println(sms)//toString
    sms.component1()//time
    sms.component2()//content
    sms.component3()//from
    sms.time
}
//get
//set
//hashcode
//tostring
//equals
//copy
//构造函数
data class Sms(var time:Long,var content:String,var from:String)

