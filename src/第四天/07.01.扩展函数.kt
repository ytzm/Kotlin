/**
 * 可以代替项目开发中的各种util
 * 不需要定义新的对象就可以添加新的函数
 * 父类定义扩展函数  子类可以直接使用
 */
fun main(args: Array<String>) {
    val s:String = ""
    s.isEmpty()
}
//判断字符串是否为空
fun String.isEmpty(){
    if(this==null||this.length==0){
        println("为空")
    }else{
        println("不为空")
    }
}




