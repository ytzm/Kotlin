fun main(args: Array<String>) {
    val son = Son()
    val person = SmallFather(son)
    person.wash()
}

//洗碗能力
interface WashPower{
    fun wash()
}

class Son: WashPower {
    override fun wash() {
        println("大头儿子洗碗了")
    }
}
//小头爸爸也有洗碗能力了
class SmallFather(var son:Son):WashPower by son


