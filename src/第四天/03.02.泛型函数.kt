package 第四天面向对象
fun main(args: Array<String>) {
//    parseType(10)
//    parseType("张三")
    parseType('a')
    parseType(true)
}
//传递任意数据  判断数据类型
//"haha"  10 'a'
//不知道具体传递什么类型  就可以使用泛型
fun <T> parseType(param:T){
    when(param){
        is String-> println("是字符串类型")
        is Int-> println("是Int类型")
        is Char-> println("是Char类型")
        else-> println("不知道具体类型")
    }
}

