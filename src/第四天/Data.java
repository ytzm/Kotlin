package 第四天;

public class Data {
    private String content;
    private String from;
    private Long time;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Data(String content, String from, Long time) {
        this.content = content;
        this.from = from;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Data{" +
                "content='" + content + '\'' +
                ", from='" + from + '\'' +
                ", time=" + time +
                '}';
    }
    //hashcode
    //eqquals
}
