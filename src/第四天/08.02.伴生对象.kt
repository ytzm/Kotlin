/**
 * 伴生对象
 * 定义:companion object InClass  名字可以不写
 * 调用伴生对象里面字段:
 * MyData.sayHello()
MyData.InClass.sayHello()
 */
fun main(args: Array<String>) {

    //访问伴生对象里面字段
    println(MyData.name)
    println(MyData.name)

    MyData.sayHello()
    MyData.InClass.sayHello()

    val myData = MyData()
    myData.age
}

class MyData {
    var age = 20  //需要创建对象访问
    //伴生对象  可以直接访问
    companion object InClass{
        var name = "张三"
        fun sayHello(){
            println("你好")
        }
    }
}


