fun main(args: Array<String>) {
    val smallFather = SmallFather1()
    smallFather.wash()
}
//洗碗能力
interface WashPower1{
    fun wash()
}
//大头儿子已经具备了洗碗能力
class Son1:WashPower1{
    override fun wash() {
        println("大头儿子洗碗了")
    }
}
//小头爸爸  可以把洗完能力委托给大头儿子实现
class SmallFather1:WashPower1 by Son1()



