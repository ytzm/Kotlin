
fun main(args: Array<String>) {
    val inClass = OutClass2().InClass()
    inClass.sayHello()
}
//this@OutClass2  外部类的this
class OutClass2 {
    var name = "张三"
    inner class InClass {
        var name = "李四"
        fun sayHello() {
//            println(name)
            //访问外部类name字段  OutClass2.this
            println(this@OutClass2.name)
        }
    }
}