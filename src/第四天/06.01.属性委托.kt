import kotlin.reflect.KProperty

fun main(args: Array<String>) {
    val bigSon = BigSon()
    //爷爷给了100块钱
    bigSon.压岁钱 = 100
    //儿子直接使用压岁钱
    println(bigSon.压岁钱)
}
/**
 * 属性委托就是把自己字段的get和set方法交给其它类实现
 */
class BigSon {
    //字段的get和set方法交给其它类实现
    //将儿子的压岁委托给Father
    var 压岁钱:Int by Father()
}
//将压岁钱交给父亲管理
class Father{
    //负责管理儿子的压岁钱
    var 儿子的压岁钱:Int = 0

//    operator fun getValue(bigSon: BigSon, property: KProperty<*>): Int {
//        return 1
//    }
//
//    operator fun setValue(bigSon: BigSon, property: KProperty<*>, i: Int) {}

    operator fun getValue(bigSon: BigSon, property: KProperty<*>): Int {
        println("调用了父类的getValue")
        return 儿子的压岁钱-50
    }

    operator fun setValue(bigSon: BigSon, property: KProperty<*>, i: Int) {
        println("调用了父类的setValue")
        this.儿子的压岁钱 = 100
    }



}
