package 第四天面向对象

/**
 * 中缀表达式
 * 类的函数加上infix关键字
 * 函数参数只能有一个
 * 调用函数 对象  函数名  参数
 * 中缀表达式更加符合人的思维习惯
 * dsl
 * ,开发人员通过中缀表达式  自定义关键字
 */
fun main(args: Array<String>) {
    val person = Person2()
//    person.hello("张三")
    person hello "张三"
}
class Person2{
    infix fun hello(name:String){
        println("hello $name")
    }
}