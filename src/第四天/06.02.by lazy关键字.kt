/**
 * 延迟加载  当用到时才会加载 只会加载一次  线程安全的
 * 只创建一次  并且用到时再创建
 */
val lazyValue: String by lazy {//最后一行是返回值
    println("执行了初始化lazyValue")
    "张三"
}

fun main(args: Array<String>) {
    println(lazyValue)
    println(lazyValue)
}
