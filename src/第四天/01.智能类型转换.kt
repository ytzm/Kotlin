/**
 * java
 * 如果已经判断是当前类型之后  编译器就会自动将对象转换为当前类型 不需要手动转换
 */
fun main(args: Array<String>) {
    val animal1: IAnimal = IDog()
    val animal2: IAnimal = ICat()

//    animal1.wangwang()
    //animal1调用wangwang
    //1.判断animal1是否是Idog类型
    if (animal1 is IDog) {//相当于java的instanceof 判断是否是这个类型
        //2.如果是IDog类型  将animal1强转成IDolg
//        val animal1N = animal1 as IDog  //将animal1强转成IDog类型
        //3.调用wangwang方法
        animal1.wangwang()
    }
}

open class IAnimal

class IDog : IAnimal() {
    fun wangwang() {
        println("狗汪汪叫")
    }
}

class ICat : IAnimal() {
    fun miaomiao() {
        println("猫喵喵叫")
    }
}