package 第四天面向对象

/**
 * 定义时不确定具体类型  只用使用时才知道具体类型
 */
//定义一个箱子
open class Box<T>(var thing:T)

//子类继承Box  知道具体泛型类型
class SonBox(thing:Int):Box<Int>(thing)
//如果子类不知道具体泛型类型  可以定义泛型传递
class SonBox2<D>(thing:D):Box<D>(thing)

fun main(args: Array<String>) {
    //创建可以传递Int类型的Box
    val intBox = Box<Int>(10)
    //创建可以传递String类型的 Box
    val stringBox = Box<String>("张三")

}