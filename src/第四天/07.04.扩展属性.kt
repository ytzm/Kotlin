/**
 * 扩展属性
 */
fun main(args: Array<String>) {
    val student = Student("张三",30)
//    student.email = """qq.com"""
    println(student.phone)
//    println(student.email)
}

class Student(var name:String,var age:Int)

//定义了扩展属性 phone
val Student.phone
get() = "13567893456"

//var Student.email:String
//get():String {
//    return email
//}
//set(value) {
//    email=value
//    }


