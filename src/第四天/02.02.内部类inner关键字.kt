/**
 * inner关键字  内部类(相当于java的内部类)
 * 内部类依赖外部类的环境  可以访问外部类的字段或者函数
 */
fun main(args: Array<String>) {
    val inClass = OutClass1().InClass()
}

class OutClass1 {
    var name = "李四"
    inner class InClass {
        fun sayHello() {
            println(name)
        }
    }
}