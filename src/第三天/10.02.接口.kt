package 第三天
/**
 * 1.抽象类:事物的共同特性 代表的就是事物的本质  一个类只能继承一个抽象类
 * 2.接口:代表的是事物具备的能力 能力可以多实现
 * 类只能继承一个抽象类
 * 类可以实现很多接口
 */
fun main(args: Array<String>) {
    val perosn = ZhHuman2("张三",30)
    perosn.rideBike()

    perosn.driveCar()
}
//人类  abstract class抽象类
abstract class Human3(var name:String,var age:Int){
    //吃饭 不知道具体实现 方法前加上abstract
    abstract fun eat()
}
//中国人  用筷子吃饭
class ZhHuman2(name: String,age: Int):Human3(name, age),Ride,Drive{
    override fun driveCar() {
        println("学会开汽车了")
    }

    override fun rideBike() {
        println("学会骑自行车了")
    }

    override fun eat() {
        println("用筷子吃饭")
    }
}
//骑自行车能力 接口
interface Ride{
    fun rideBike()
}
//开汽车能力 接口
interface Drive{
    fun driveCar()
}



