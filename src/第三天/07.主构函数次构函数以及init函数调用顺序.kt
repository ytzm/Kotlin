package 第三天

/**
 * 调用次构函数也会执行init里面代码
 * 调用次构函数创建对象时 先执行init  再执行次构代码
 */
fun main(args: Array<String>) {
    val person1 = Person5("张三",20,"12333")
//    val person2 = Person5("李四",30,"34455")
//    val person3 = Person5("王五",40,"4565666")
//    val person4 = Person5("赵六",50,"46666")
}
//先次构  再init  1
class Person5(var name:String,var age:Int){
    var phone = ""
    constructor(name:String,age:Int,phone:String):this(name, age){//次构函数也会调用主构函数
        this.phone =  phone
        println("调用了次构函数")
    }
    init {
        println("创建了person5")
    }
}
