package 第四天面向对象

fun main(args: Array<String>) {
    val person = Person("张三",30)
    val person2 = Person("李四",40)
    println("${person.name}")
    person.name = "李四"
    println("${person2.name}")
}
//创建人同时通过构造方法传递name和age
class Person(var name:String,var age:Int)//对象的主构造函数

