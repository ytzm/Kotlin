package 第三天

/**
 * 构造函数字段使用val只能访问 不能修改
 */
fun main(args: Array<String>) {
    val person = Person2("张三",30)
    //访问字段
    val name = person.name
    val age = person.age
    //修改字段
//    person.name = "李四"
}
class Person2(val name:String,val age:Int)
