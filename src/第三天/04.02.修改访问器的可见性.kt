package 第三天

/**
 * 修改访问器的可见性
 */
fun main(args: Array<String>) {
    val perosn= Person3()
    perosn.name//可以访问
//    perosn.name = "李四"//可以修改

//    perosn.age
//    perosn.age=30
}
class Person3 {
    var name: String = "张三"
    private set //不实现setName方法

    private var age = 30//getAge和setAge都不会实现

    private fun sayHello(){
        name = "李四"
    }
}
