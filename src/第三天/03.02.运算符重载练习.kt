package 第三天

fun main(args: Array<String>) {
    var a = 10
    a++//11
    //定义老师实例
    var 助教 = Teacher()
    println("助教等级=${助教.level} 助教工资=${助教.salary}")
//    助教.level = 助教.level+1
//    助教.salary = 助教.salary+500
    //++ 讲师
    val 讲师 = 助教++
    println("讲师等级=${讲师.level} 讲师工资=${讲师.salary}")
}
//老师
class Teacher{
    var level = 1
    var salary  = 1000
    //实现inc()函数就可以实现++
    /**
     * operator 运算符重载关键字
     */
    operator fun inc():Teacher{
        level++
        salary += 500
        return this
    }
    //实现dec()函数就可以实现--
    operator fun dec():Teacher{
        level--
        salary -= 500
        return  this
    }
    //实现plus()函数就可以实现+
    operator fun plus(times:Int):Teacher{
        level += times
        salary += times * 500
        return  this
    }
    //实现minus()函数就可以实现-
    operator fun minus(levels:Int):Teacher{
        if(levels > 0){
            level -= levels
            salary -= levels * 500
            return  this
        }
        return this
    }
}

