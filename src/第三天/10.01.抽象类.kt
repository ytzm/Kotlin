package 第三天

/**
 * 抽象类不需要加open关键字也可以被继承
 * 抽象方法也不需要加open关键字
 * 有abstract方法  子类必须实现
 */
fun main(args: Array<String>) {
    val zhHuman = ZhHuman("张三那",20)
    val usHuman = ZhHuman("李四那",30)
    println(zhHuman.eat())
    println(usHuman.eat())
}
//人类  abstract class抽象类
abstract class Human(var name:String,var age:Int){
    //吃饭 不知道具体实现 方法前加上abstract
    abstract fun eat()
}
//中国人  用筷子吃饭
class ZhHuman(name: String,age: Int):Human(name, age){
    override fun eat() {
        println("用筷子吃饭")
    }
}
//美国人 用刀叉吃饭
class UsHuman(name: String, age: Int) : Human(name, age){
    override fun eat() {
        println("用刀叉吃饭")
    }

}

open class HumanH

class ddd:HumanH{//()不是必须的
    constructor():super()
}

abstract class HumanT
class eee:HumanT{
    constructor()//证明了抽象类没有构造函数
}

