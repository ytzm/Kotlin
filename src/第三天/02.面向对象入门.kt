package 第三天

/**
 * 8基本数据类型
 * string字符串
 *
 */
fun main(args: Array<String>) {
    //创建Person实例  李四  40
    val person = Person()
    person.name = "李四"
    person.age = 40
    val name = person.name
    val age = person.age
    println("name=$name age=$age")
}

//人 名字  年龄
class Person{
    var name:String = "张三"
    var age:Int = 30
}
