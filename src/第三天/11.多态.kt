package 第三天

/**
 * 同种功能的不同表现形式
 * 通过父类类型接收子类对象  还是会执行子类的方法
 */
fun main(args: Array<String>) {
    //通过父类类型接收子类类型
    val dog:Animal = Dog()
    val cat:Animal = Cat()

    //动物叫  1   汪汪汪 2
    dog.call()
    cat.call()
}
//动物
open class Animal{
    open fun call(){
        println("动物叫")
    }
}
//狗
class Dog: Animal() {
    override fun call() {
        println("汪汪汪")
    }
}
//猫
class Cat: Animal(){
    override fun call() {
        println("喵喵喵")
    }
}