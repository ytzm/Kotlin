package 第三天

/**
 * + kotlin运算符其实对应的是函数
 * -
 * *
 * /
 *
 */
fun main(args: Array<String>) {
    var a = 10
    var b = 20
    //Ctrl
    a + b
    val sum = a.plus(b)
    a - b
    a * b
    a / b
    val range = 1..100
    1.rangeTo(100)

    a++
    a--

    val person1 = Person()
    val person2 = Person()
    //通过运算符重载可以实现对象加减乘除  课下实现
//    person1+person2
}

