package 第三天

fun main(args: Array<String>) {
    val man = Man("张三",30)
    println(man.name)
    println(man.age)
}
open class Human1(var name:String,var age:Int)
/**
 * 如果主构函数里面有字段
 * 子类必须传递字段给符类
 */
class Man(name: String, age: Int) : Human1(name, age)


