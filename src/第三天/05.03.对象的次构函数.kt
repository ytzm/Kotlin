package 第四天面向对象

fun main(args: Array<String>) {
    //调用主构函数创建person对象
    val person1 = Person4("张三",30)
    //调用次构函数创建person对象
    val person2 = Person4("李四",40,"13456789000")

    person2.name
    person2.name = "王五"


    //次构函数独有的字段访问
    person2.phone = "1345666666"
    println(person2.phone)
}
//人  姓名 年纪  电话号码
class Person4(var name:String,var age:Int){//对象的主构造函数
    var phone = ""
    //定义次构函数
    constructor(name:String,age:Int,phone:String):this(name, age){//后面:this调用主构函数
        this.phone = phone
    }
}
