package 第四天面向对象

/**
 * 对于任意字段都有init
 * 每一次创建对象都会执行init里面代码
 * 可以把一些初始化的操作放在init里面实现
 */
fun main(args: Array<String>) {
    val person1 = Person5("张三",20)
    val person2 = Person5("李四",30)
    val person3 = Person5("王五",40)
    val person4 = Person5("赵六",50)
}
class Person5(var name:String,var age:Int){
    init {
        println("创建了person5")
    }
}