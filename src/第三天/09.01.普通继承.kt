package 第三天

/**
 * 继承 父类需要加上open
 * 继承用:继承父类
 * 子类可以使用父类的字段和方法
 */
fun main(args: Array<String>) {
    //定义Son对象
    val son = Son()
    //可以访问父类里面的字段
    println(son.name)
    println(son.age)
    //调用父类里面的函数
    son.sayHello()
}
//需要加上open才能被继承
open class Father{//默认final的class 不能被继承
    var name:String = "父亲"
    var age:Int = 40
    fun sayHello(){
        println("父类hello")
    }
}
//儿子继承父亲  :继承以及接口实现
class Son:Father(){

}


