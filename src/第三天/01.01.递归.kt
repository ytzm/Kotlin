package 第三天
//包子馅的包子  最后就是馒头
fun main(args: Array<String>) {
    //n的阶乘  n*(n-1)*..1
    //斐波那契数列   1 1 2 3 5 8 13 21 34 55
    //求第n个斐波那契数列
    println(feiBo(8))

    //1..n的和  1+2+3+4+5
    //1+2+3+4  求1到n-1的和  +5
    println(sum(10))
}
//求1到n的和
fun sum(n:Int):Int{
    if(n==1)return 1
    return sum(n-1)+n
}

//求第n个斐波那契数列
//将复杂的问题转换成一个比较简单的相同的问题
fun feiBo(n:Int):Int{
    if(n==1) return 1
    if(n==2) return 1
    //如果大于2  就是第n-1 和第n-2个斐波那契数列之和
    return feiBo(n-1)+ feiBo(n-2)
}



