package 第三天
/**
 * 1.复写父类的属性字段和方法  必须要将父类的字段和方法加上open
 * 2.子类里面字段和方法就需要加上override关键字
 */
fun main(args: Array<String>) {
    val son  =Son1()
    //获取儿子的名称
    println(son.name)
    son.sayHello()
}
open class Father2{
    //如果需要让子类继承 需要加上open关键字
    open var name = "小头爸爸"
    var age = 30
    open fun sayHello(){
        println("父亲hello")
    }
}
//儿子继承父亲  :继承以及接口实现
class Son1: Father2() {
    //override继承父类的字段
    override var name: String = "大头儿子"

    override fun sayHello() {
//        super.sayHello()
        println("儿子hello")
    }
}

