package 第三天

/**
 * 一般情况下递归和迭代都可以实现功能
 * 递归方式如果层级比较多 会报StackOverflowError 栈内存溢出
 * 迭代方式不会报错
 * 递归内存开销比较大
 * 迭代内存开销比较小,但写起来麻烦
 *
 * 如果要写起来简单并且效率比较高  就需要进行递归优化
 * 在java没有尾递归优化
 * 只有是尾递归才能优化
 *尾递归:调用当前函数没有做其他任何操作称为尾递归
 * tailrec :对尾递归进行优化
 *
 * 尾递归优化:将递归转换为迭代进行实现
 */
fun main(args: Array<String>) {
    //通过递归方式1到n
    println(sum1(100000))
    //通过循环求1到n的和
//    println(sum2(100000))
}
//求1到n的和
//fun sum2(n:Int):Int{
//    var copyN = n
//    var result = 0//接收和
//    while (copyN>0){
//        result = result+copyN
//        copyN--
//    }
//    return result
//}

//求1到n的和
tailrec fun sum1(n:Int,b:Int=0):Int{
    if(n==1)return b+1
//    return sum1(n-1)+n//不是尾递归
    return sum1(n-1,b+n)//尾递归
}

