package 第三天

fun main(args: Array<String>) {

}
//人类  abstract class抽象类
abstract class Human4(var name:String,var age:Int){
    //吃饭 不知道具体实现 方法前加上abstract
    abstract fun eat()
}
//中国人  用筷子吃饭
class ZhHuman3(name: String,age: Int):Human4(name, age),Ride1,Drive1{
//    override fun driveCar() {
//        println("学会开汽车了")
//    }

    override fun rideBike() {
        println("学会骑自行车了")
    }

    override fun eat() {
        println("用筷子吃饭")
    }
}
//骑自行车能力 接口
interface Ride1{
    fun rideBike()
}
//开汽车能力 接口
interface Drive1{
    //直接在接口中实现当前能力
    fun driveCar(){
        println("学会开车了")
    }
}
