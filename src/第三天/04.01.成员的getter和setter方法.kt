package 第三天

/**
 * kotlin里面定义好成员变量之后  就已经默认实现了get和set方法
 */
fun main(args: Array<String>) {
    val person = Person1()
    //获取person对象里面的name值
    println(person.name)//getName
    //修改了person里面的name字段值
    person.name = "李四"//setName
    println(person.name)
}
class Person1 {
    //成员变量
    var name: String = "张三"
    val age = 30
}
