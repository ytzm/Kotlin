package 第一天

/**
 * equals  笔记字符串值是否相等
 * == 相当于equals
 * ===相当于java的== 比较内存地址
 */
fun main(args: Array<String>) {
    /*---------------------------- 字符串比较 ----------------------------*/
    var s1 = "hello"
    var s2 = String(charArrayOf('h','e','l','l','o'))
//    println(s1)
//    println(s2)
    //equals
    //== 相当于是equals
    println(s1.equals(s2))//字符串的值
    println(s1==s2)//true  1  false 2
    println(s1===s2)
}