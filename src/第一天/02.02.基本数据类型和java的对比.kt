package 第一天
/**
 * java基本数据类型
 *  包装数据类型Integer
 *  kotlin只有8中数据类型没有包装数据类型
 *  kotlin自动根据使用场景选择使用基本数据类型还是包装数据类型
 *  高度模板化的java
 */
fun main(args: Array<String>) {
    var a: Int = 10 //对应java基本数据类型
    a.hashCode()  //对应java的Integer类型

    //查看kotlin对应的java代码---Tools--Kotlin--Show Kotlin Bytecode
    println(a.hashCode())//自动使用了Integer
}