package 第一天

fun main(args: Array<String>) {
    /*---------------------------- 输出函数 ----------------------------*/
    val a = 10
    val b = 20
//    println("a="+a+"b="+b)
    println("a=$a b=$b")

    /*---------------------------- 输入函数 ----------------------------*/
    var s:Int = 0
    //从控制台输入数据
    s = readLine()?.toInt()?:-1
//    println(s)
}