package 第一天

/**
    Byte.MIN_VALUE:-128
    Byte.MAX_VALUE:127
    Short.MIN_VALUE:-32768
    Short.MAX_VALUE:32767
    Int.MIN_VALUE:-2147483648
    Int.MAX_VALUE:2147483647
    Long.MIN_VALUE:-9223372036854775808
    Long.MAX_VALUE:9223372036854775807
    Float.MIN_VALUE:-3.4028235E38
    Float.MAX_VALUE:3.4028235E38
    Char.MAX_HIGH_SURROGATE:?
    Char.MAX_LOW_SURROGATE:?
    Char.MAX_SURROGATE:?
    Char.MIN_HIGH_SURROGATE:?
    Char.MIN_LOW_SURROGATE:?
    Char.MIN_SURROGATE:?
    Double.MAX_VALUE:1.7976931348623157E308
    Double.MIN_VALUE:4.9E-324
    Double.NEGATIVE_INFINITY:-Infinity
    Double.NaN:NaN
    Double.POSITIVE_INFINITY:Infinity
 */
fun main(args: Array<String>) {
    var minB: Byte = Byte.MIN_VALUE
    var maxB: Byte = Byte.MAX_VALUE
    println("Byte.MIN_VALUE:${minB}")
    println("Byte.MAX_VALUE:${maxB}")

    var minS = Short.MIN_VALUE
    var maxS = Short.MAX_VALUE
    println("Short.MIN_VALUE:${minS}")
    println("Short.MAX_VALUE:${maxS}")

    var maxI = Int.MAX_VALUE
    var minI = Int.MIN_VALUE
    println("Int.MIN_VALUE:${minI}")
    println("Int.MAX_VALUE:${maxI}")

    var maxL = Long.MAX_VALUE
    var minL = Long.MIN_VALUE
    println("Long.MIN_VALUE:${minL}")
    println("Long.MAX_VALUE:${maxL}")


    //浮点类型
    var maxF:Float = Float.MAX_VALUE
    var minF:Float = -Float.MAX_VALUE
    println("Float.MIN_VALUE:${minF}")
    println("Float.MAX_VALUE:${maxF}")

    println("Char.MAX_HIGH_SURROGATE:"+Char.MAX_HIGH_SURROGATE)
    println("Char.MAX_LOW_SURROGATE:"+Char.MAX_LOW_SURROGATE)
    println("Char.MAX_SURROGATE:"+Char.MAX_SURROGATE)
    println("Char.MIN_HIGH_SURROGATE:"+Char.MIN_HIGH_SURROGATE)
    println("Char.MIN_LOW_SURROGATE:"+Char.MIN_LOW_SURROGATE)
    println("Char.MIN_SURROGATE:"+Char.MIN_SURROGATE)


    println("Double.MAX_VALUE:"+Double.MAX_VALUE)
    println("Double.MIN_VALUE:"+Double.MIN_VALUE)
    println("Double.NEGATIVE_INFINITY:"+Double.NEGATIVE_INFINITY)//Infinity : 无限 NEGATIVE_INFINITY代表负无穷大
    println("Double.NaN:"+Double.NaN)
    println("Double.POSITIVE_INFINITY:"+Double.POSITIVE_INFINITY)//POSITIVE_INFINITY代表负正穷大

}
