package 第一天

fun main(args: Array<String>) {
    val str = "    abcdef   "
    println(str)
    /*---------------------------- 去除空格 ----------------------------*/
    val newStr = str.trim()
    println(newStr)
    /*---------------------------- 原样输出字符串去除空格 ----------------------------*/
    val s = """
        ;中国
        ;广东省
        ;深圳市
    """.trimMargin(";")//字符串|前面的空格全部去除
    println(s)
}