package 第一天

/**
 * Alt+Enter提示错误处理方式
 * 空值处理
 * null空
 * val s:String s不为空的类型 不能赋值为null
 * val s: String? s是可空类型  可以赋值为null
 * ?.  空安全调用符
 * !!  非空断言 告诉编译器当前s一定不为null 如果s为null还会包空指针异常(不要使用)
 */
fun main(args: Array<String>) {
    //定义字符串
    val s: String? = null
    //字符串转换为Int类型
//    if(s!=null){
//        return s.toInt()
//    }else{
//    return null
//        }
    //空安全调用符
//    s?.toInt()//s如果不为null执行后面toInt  为null就不执行返回null
    //非空断言
//    s!!.toInt()

    //?:Elvis操作符
    //Int? 可空类型
    val a:Int = s?.toInt()?:-1//如果s不为空 将s.toInt值返回  如果为null返回-1
    println(a)
}

