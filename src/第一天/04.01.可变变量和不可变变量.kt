package 第一天
/**
 * var  可变变量  可以重新赋值
 * val(value) 不可变变量  只能赋值一次
 * 建议首选使用val 相当于是java的final
 */
fun main(args: Array<String>) {
    var a:Int = 10
    a = 20
    a = 30

    val b:Int = 20
//    b = 30
}