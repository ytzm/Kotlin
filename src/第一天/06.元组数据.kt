package 第一天

/**
 * 姓名
 * 年纪
 */
/**
 * 元组数据
 * 二元元组 2个属性 Pair
 * 三元元组 3个属性
 * 二元元组定义
 * 第一种:Pair("张三",30)
 * 第二种:"张三" to 30
 * 三元元组定义: Triple("张三",30,"14567890000")
 */
fun main(args: Array<String>) {
    /*---------------------------- 二元元组 ----------------------------*/
    //张三  30
//    val person = Pair("张三",30)
    val person = "张三" to 30
    val name = person.first
    val age = person.second
    println(name)
    println(age)

    /*---------------------------- 三元元组 ----------------------------*/
    val newPerson = Triple("张三",30,40)
    val newName = newPerson.first
    val newAge = newPerson.second
    val newPhone = newPerson.third
    println(newName)
    println(newAge)
    println(newPhone)

}
