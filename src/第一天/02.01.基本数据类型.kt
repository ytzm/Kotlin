package 第一天
//ctrl+/自动补全,程序入口(主函数)前面有kotlin图标 单击此图标,执行Run
//ctrl+D删除一行
//commond+↓ 复制
//shift+↑ 向上交换行
//shift+↓ 向下交换行
//option+Enter 自动修复提示
fun main(args: Array<String>) {
    //8大基础数据类型
    //注意:与java不同,①Kotlin数据类型均为大写开头 ②无包装类型(小范围赋值给大范围不会自动隐式类型转换)
    var bool: Boolean = true
    var byte: Byte = 1
    var short: Short = 20
    var char: Char = 'a'
    var int: Int = 100
    var float: Float = 1.12345f
    var double: Double = 1.12345
    var long: Long = 10L

    var _firstName = """咕咕咕咕"""
    println(message = _firstName)

    //String 字符串
    var string: String = "张三"
    //Boolean直接和字符串拼接会报错?----由于Kotlin没有基本数据类型,全都是包装类型,有toInt()等方法也说明了它是对象,+也会自动转为plus()方法,只有String,Char的plus()才可以接收字符串
    println(bool.toString()+"\n"+byte+"\n"+short+"\n"+char+"\n"+int+"\n"+float+"\n"+double+"\n"+long+"\n"+string)

    print("" + true)
//    print(true + "")//报错
//    print(3 + "")//报错
    print("" + "3")
    println('a'+3)
//    println(''+3)//报错
    println('a' + "good")

}


