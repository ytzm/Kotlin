package 第一天

fun main(args: Array<String>) {
    //定义姓名张三
    val name = "张三"
    val from = "中国\n广东省\n深圳市" //\n换行符
    println(from)

    /*---------------------------- 原样输出字符串 ----------------------------*/
    val newFrom = """
        中国
        广东省
        深圳市
    """.trimIndent()
    println(newFrom)
}