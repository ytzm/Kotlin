package 第一天

fun main(args: Array<String>) {
    /*---------------------------- 智能类型推断 ----------------------------*/
    //int a = 10;
//    var a:Int = 10
    var a = 10  //自动推断出a是Int类型
    a = 20
//    a="zhangsan"

    var c = 'c'//自动推断出是char类型
    c='z'
//    c=10

    /*---------------------------- 类型转换 ----------------------------*/
    var b = 10  //b是Int类型
    var s = "20"//s是String字符串类型
    b = s.toInt()
    c.toInt()
    b.toLong()
    println(b)
}