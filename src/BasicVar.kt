fun main(args: Array<String>) {
    //变量基础类型
    val time: Int = 3
    //变量插入到文本-----${}变量占位符
    println("北京时间${time}点整")
    //由于kotlin有优秀的类型推断系统,:Int可省略
    var time2 = 3

    var 一个整数 = 1237
    var 一个长整数 = 12666L
    var 一个双精度浮点数 = 1237.99
    var 一个单精度浮点数 = 1237.34F
    //kotlin不支持八进制
    var 一个二进制数 = 0b0110
    var 一个十六进制数 = 0xACF
    //数太大,可用_分割位数,便于阅读
    var money = 100_0000

    //注意:不能自动的向上扩展数字范围(不仅不能大赋小,小赋大也不可以)
    val int1 = 999
    val long1 = int1.toLong()
    //位移运算也是用函数来实现的
    val 左移2 = 1 shl 2
    val 右移2 = 1 shr 2
    val 无符号右移2 = 1 ushr 2
    val 与 = 1 and 2
    val 或 = 1 or 2
    val 异或 = 1 xor 2
    val 取反 = 1.inv()

    //Double转Int会截断小数部分,而不是四舍五入
    println("3.14转整数:"+3.14.toInt())

    //类型安全(不匹配会报错)------js也支持类型推断,但有隐患,kotlin与swift,TypeScript解决了此问题
//    var 电费 = 0.5
//    电费 = "五毛"

    //kotlin建议尽量用常量,提示出错再改回变量

    /**
     * C语言0代表假,非0代表真
     * kotlin  Boolean只有true和false
     */

    //元组:把多个不同类型的值赋给一个变量,以.first .second .third 来引用---超过3个数据用对象解决
    val 课程 = Triple(3,"学会","kotlin")
    val 费用 = Pair("学费",0)
    print("${课程.first}天${课程.second}${课程.third},${费用.first}${费用.second}元!")

    //可空类型(nullable)---类似swift中的Optional(选填)
    var sex: Boolean?
    sex = false
    if(sex != null && sex == true){
        print("先生,已经最便宜了")
    }else{
        print("美女,全场八折哦!")
    }

    //空格问题---一元操作符无需空格,二元操作符需要空格,()无需空格
    var a = 3
    val b = -a
    print(a >= b)

}