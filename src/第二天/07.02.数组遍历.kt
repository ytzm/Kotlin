package 第二天

fun main(args: Array<String>) {
    val arr1 = arrayOf("张三","李四","王五")
    //for
//    for (a in arr1){
//        println(a)
//    }
//    for ((index,a) in arr1.withIndex()){
//        println("index=$index any=$a")
//    }
    //foreach
//    arr1.forEach {
//        println(it)
//    }
    arr1.forEachIndexed { index, s ->
        println("index=$index s=$s")
    }
}