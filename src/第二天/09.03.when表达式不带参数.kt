package 第二天

/**
 * kotlin的when表达式可以不添加参数
 * 语法糖
 */
fun main(args: Array<String>) {
    var age = 7
    val result = todo3(age)
    println(result)
}
//判断我们现阶段在做什么
fun todo3(age:Int):String{
    when{
        age==7-> return "开始上小学了"  //如果只有一行可以省略{}

        age==12->{
            return "开始上中学了"
        }
        age==15->{
            return "开始上高中了"
        }
        age==18->{
            return "开始上大学了"
        }

        age is Int->return "传递的是int数据类型"

        else->{
            return "开始上社会大学"
        }
    }
}


