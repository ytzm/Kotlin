package 第二天

/**
 * 只能在for循环中
 * break  跳出循环  后面不再执行
 * continue  跳出当前循环  后面还会继续执行
 */
fun main(args: Array<String>) {
    var s = "abcde"
    //遍历字符串
    for (c in s){
        //如果字符是'c' 后面就不用输出了   a b
        if(c=='c'){
//            break  //跳出了循环  后面不会再执行
            continue
        }
        println(c)
    }
}