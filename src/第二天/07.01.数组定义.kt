package 第二天

/**
 * 相同数据类型元素的集合
*/
fun main(args: Array<String>) {
    val  arr = arrayOf(1,2)
    //张三  李四  王五
    //定义数组保存三个姓名
    val array = arrayOf("张三","李四","王五")
    //Boolean
    //Byte
    //Char
    //Short
    //Int
    //Float
    //Double
    //Long
    val array2 = booleanArrayOf(true,false,true,false)
    val array3 = intArrayOf(1,2,3)
}
