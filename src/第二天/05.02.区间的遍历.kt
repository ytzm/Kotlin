package 第二天

fun main(args: Array<String>) {
    val range = 1..10
    //for
//    for (a in range){
//        println(a)
//    }
    //for循环 打印角标
//    for ((index,a) in range.withIndex()){
//        println("index=$index a=$a")
//    }
    //foreach
//    range.forEach {
//        println(it)
//    }
    //foreach打印角标
    range.forEachIndexed { index, i ->
        println("index=$index i=$i")
    }
}