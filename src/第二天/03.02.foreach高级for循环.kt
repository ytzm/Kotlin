package 第二天

fun main(args: Array<String>) {
    val s = "我是中国人"
    //通过foreach遍历字符串
    s.forEach {
        println(it)//it代表每一个字符
    }

    //打印字符和角标
    /**
     * index 角标
     * c 每一个字符
     */
    s.forEachIndexed { index, c ->
        println("index=$index c=$c")
    }
}