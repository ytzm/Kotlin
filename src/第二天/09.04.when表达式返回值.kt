package 第二天

/**
 * when表达式有返回值  返回值就是{}里面最后一行
 * switch 如果参数判断简单就是switch  复杂就是if else
 */
fun main(args: Array<String>) {
    var age = 7
    val result = todo4(age)
    println(result)
}

//判断我们现阶段在做什么
fun todo4(age: Int): String {
    return when (age) {
        7 -> {//返回值就是{}里面最后一行
            println("找到了结果了")
            10
            "开始上小学了"  //如果只有一行可以省略{}
        }

        12 ->  "开始上中学了"

        15 ->  "开始上高中了"

        18 ->  "开始上大学了"

        else ->  "开始上社会大学"
    }
}

