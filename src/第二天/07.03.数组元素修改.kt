package 第二天

fun main(args: Array<String>) {
    var newArr = arrayOf(1,2,3,4,2,3,5)
    //把角标为3的元素改成10
//    val ele = newArr[3]
//    println(ele)
    newArr[3] = 10
    println(newArr[3])

    //第二种方式修改
    newArr.set(3,20)// 将数组角标为3的元素修改为20
    println(newArr[3])
}