package 第二天

/**
 * when分支判断语句
 * switch类似  char short int 枚举  字符串
 * when表达式可以支持任意类型以及条件
 */
fun main(args: Array<String>) {
    var age = 12
    val todo = todo(age) //无返回值函数返回kotlin.Unit
    println(todo)
}
//判断我们现阶段在做什么
fun todo(age:Int){
    when(age){
        7-> println( "开始上小学了") //如果只有一行可以省略{}
        is Int -> println( "年龄是整数")//return了
        12->println( "开始上初中了")//不执行
        15->println( "开始上高中了")
        18->println( "开始上大学了")
        else->println( "开始闯社会了")//else取代default
    }
}

