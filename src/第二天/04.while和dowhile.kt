package 第二天

/**
 * while 条件循环 条件不满足 不会执行
 * wo while 如果条件不满足会执行一次
 */
fun main(args: Array<String>) {
    /*---------------------------- while ----------------------------*/
    //将小于100的整数全部打印
    var i = -1
    var sum = 0//接收和
    while (i>0){//只要i>0满足条件 就会执行
//        sum = sum + i
//        i-- //100 -1  99-1
        println("执行了wihle循环")
    }
//    println(sum)
    /*---------------------------- do while ----------------------------*/
    do {
//        sum = sum + i
//        i-- //100 -1  99-1
        println("执行了do while循环")
    }while (i>0)
    println(sum)

}