package 第二天

/**
 * kotlin的if有返回值  {}最后一行为返回值
 */
fun main(args: Array<String>) {
    val b = 20
    val a = 15
    val result =findBig(a, b)
    println(result)
}
//找到a和b的最大值
fun findBig(a:Int,b:Int):Int{
    //标准写法
//    if(b>a){
//        return b
//    }else{
//        return a
//    }
    //如果if语句处理只有一行
//    if(b>a)
//        return b
//    else
//        return a
    //更加简化
//    if(b>a) return b else return a
    //更加简化
    return if(b>a) b else a
}



