package 第二天

/**
 * java 方法重载
 * xutil
 * volley
 */
fun main(args: Array<String>) {
    var name = "张三"
    var age = 20
    sayHello(name = "李四",age=age)//具名参数  指定给哪一个参数赋值
//    sayHello(age = age,name = name)
}
fun sayHello(name:String="张三", age:Int){//默认参数
    println("hello  $name 你$age")
}