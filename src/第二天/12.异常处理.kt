package 第二天

//
fun main(args: Array<String>) {
    val a = 1
    val b = 0
    try {//处理程序异常 对于有可能出现异常的代码通过try catch处理
        val c = a / b//ArithmeticException   分母为0
    } catch (e: Exception) {
        println("捕获到异常了")
    }
}
