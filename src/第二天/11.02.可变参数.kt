package 第二天
fun main(args: Array<String>) {
    val a = 10
    val b = 20
    val c = 30
    val d = 40
    //求a+b的和
    println(add(a, b,c,d,30,40,50,60,20,30,100))

    test("张三","Hello,Kotlin!",'a',User(1,"liuliqianxiao"),id = 12345)
}
//可变参数 vararg
//params是数组类型
fun add(vararg params:Int):Int{
    var sum = 0
    for (ele in params){
//        sum = sum +ele
        sum += ele //和上面一句话一样
    }
    return sum
}
//fun add(a:Int,b:Int,c:Int,d:Int):Int{
//    return a+b+c+d
//}

//用Any代表任意类型
fun test(name: String,vararg args: Any, id: Int){
    println(name)
    for(it in args){
        println(it)
    }
    println(id)
}

class User(var id: Int,var name: String){
    override fun toString(): String {
        return "User(id=$id, name='$name')"
    }
}

fun addNumbers(name: String, vararg args: Int): Int {
    var result = 0
    for (i in args.indices) {
        result += args[i]
    }
    return result
}