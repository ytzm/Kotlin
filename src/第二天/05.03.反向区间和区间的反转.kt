package 第二天

fun main(args: Array<String>) {
    //反向区间
    val range = 1..10
    //10 9 8 7 6 5 4 3 2 1反向区间
//    val range1 = 10..1  不对
//    val range1 = 10 downTo 1  //定义反向区间

    //区间的反转
    val range2 = range.reversed()
    range2.forEach {
        println(it)
    }
}