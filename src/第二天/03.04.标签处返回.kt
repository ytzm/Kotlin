package 第二天

/**
 * s1和s2两两组合有几种可能
 */
fun main(args: Array<String>) {
    val s1 = "abc"
    val s2 = "123"
    //组合
    //a1  a2  a3  b1 b2 b3  c1 c2 c3
    loo@for (c in s1){
        abd@for (d in s2){
            //c = 'b'  d='2' 停止循环
            if(c=='b'&&d=='2') {
//                break//跳出里面的for循环
                //跳出外面的for循环
                break@loo  //指定跳出到标签处的for循环
//                break@abd
            }
            println("$c $d")
        }
    }
}