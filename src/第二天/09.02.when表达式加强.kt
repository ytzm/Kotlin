package 第二天

fun main(args: Array<String>) {
    var age = 13
    val result = todo2(age)
    todo(age)//调用同目录其他文件的方法
    println(result)
}
//判断我们现阶段在做什么
fun todo2(age:Int):String{
    when(age){
        in 1..6->return "还没有上学" //可以支持区间

        7-> return "开始上小学了"  //如果只有一行可以省略{}

        in 8..11->return "正在上小学"

        12->{
            return "开始上中学了"
        }

        13,14->return "正在上中学"  //可以把处理方式一样的条件放在一起

        15->{
            return "开始上高中了"
        }

        in 16..17->return "正在上高中"

        18->{
            return "开始上大学了"
        }
        is Int->return "是int数据类型"//是否是int类型

        else->{//其他情况
            return "开始上社会大学"
        }
    }
}
