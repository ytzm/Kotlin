package 第二天

fun main(args: Array<String>) {
    val s = "我是中国人"
    //将字符串每一个字符打印
    //我  是  中   国 人
    for (a in s){
        println(a)
    }

    //打印字符  并且将字符的角标一块打印
    //我  0
    //是  1
    for ((index,c) in s.withIndex()){
        println("index=$index c=$c")
    }
}