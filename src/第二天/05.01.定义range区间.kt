package 第二天

/**
 * [1,100]  包含1 包含100
 * [1,100)  包含1 不包含100 最大99
 */
fun main(args: Array<String>) {
    //定义一个从1到100的数据类型
    //java定义数组 或者集合 添加元素
    val range = 1..100  //[1,100] 最大100
    val range1 = 1 until 100 //[1,100)  最大99

    //字符区间
    //'a' 到'z'
    val charRange = 'a'..'z'
    val charRange2 = 'a' until 'z'

    //Long
    val longRange = 1L..100L

}