package 第二天

/**
 * kotlin 函数和对象都是一等公民  平等的
 */
fun main(args: Array<String>) {
    var a =10
    var b = 20
    var sum = 0
//    sum = add(a, b)
//    println(sum)

    //定义函数变量  add函数变量(c语言函数指针)
    val add:(Int,Int)->Int={a,b->a+b}//fun add(a:Int,b:Int)=a+b
    sum = add(a,b)
    println(sum)
}
//标准函数
//fun add(a:Int,b:Int):Int{
//    return a+b
//}
//如果函数只有一行 省略{} 省略return
//fun add(a:Int,b:Int):Int=a+b
//函数返回值可以省略  函数表达式
//fun add(a:Int,b:Int)=a+b


