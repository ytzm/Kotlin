package 第二天

fun main(args: Array<String>) {
    sayHello()
    sayHello("张三")
    sayHello2()
    println("Jack的名字长度为${getNameLength("Jack")}个字符")

    var x = getNameLength2()
    println(x)
}

//不可同名同参(参数个数和类型),与返回值无关
//无参无返回值函数
fun sayHello(){
    println("Hello World!")
}
//有参无返回值函数
fun sayHello(name: String){
    println("Hello $name")
}
fun sayHello(name: Int){
    println("Hello $name")
}
//无参有返回值函数
fun sayHello2():String{
    return "Hello"
}
//fun sayHello2():Int{
//    return 1
//}
//有参有返回值函数
fun getNameLength(name: String):Int{
    return name.length
}

fun getNameLength2(){//不写返回值类型则默认无返回值,并不会把最后一行语句作为返回值
    2
}