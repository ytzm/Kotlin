//option+/自动补全,程序入口(主函数)前面有kotlin图标 单击此图标,执行Run
//ctrl+D删除一行
//commond+↓ 复制
//shift+↑ 向上交换行
//shift+↓ 向下交换行
//commond+1 自动修复提示
//ctrl+shift+V 提取变量
//ctrl+shift+M 提取方法
//ctrl+shift+回车 自动补全if等
//ctrl+shift+F 自动格式化



fun main(args: Array<String>) {
    //注意:kotlin无需末尾;
    print("Hello  Kotlin!")
    println()

    //常量 val
    //变量名 : 除数字,下划线不可开头以外,均可
    //起名要---见名知意
    val π = 3.141592654
    val 女朋友数量 = 1
    val の = 0
    val girlfriendNickname = "乔恩baby"
    println(π)
    println(女朋友数量)
    println(の)
    println(girlfriendNickname)


    //变量 var
    var time = 5
    time = 10
    //变化要反应逻辑----变量值改变时,尽量用逻辑公式体现,不要直接赋新值
    time *= 2
    println(time)
    //一日3餐,每餐350g猪肉,为了减肥,每日少吃1餐,如此每年需准备多少猪肉?
    var meals = 3
    meals -= 1
    val 猪肉量 = meals * 350 *365

    print("猪肉量:"+猪肉量/1000.0+"千克")




}

