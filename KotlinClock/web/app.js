if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'app'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'app'.");
}
var app = function (_, Kotlin) {
  'use strict';
  var throwCCE = Kotlin.throwCCE;
  var Unit = Kotlin.kotlin.Unit;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var math = Kotlin.kotlin.math;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var canvas;
  var ctx;
  var width;
  var height;
  var radius;
  function main$lambda() {
    drawClock();
    return Unit;
  }
  function main(args) {
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    drawClock();
    window.setInterval(main$lambda, 1000);
  }
  function drawClock() {
    ctx.clearRect(0.0, 0.0, width, height);
    var date = new Date();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    println('hour=' + hour + ' min=' + min + ' sec=' + sec);
    drawBorder();
    drawDots();
    drawTexts();
    drawHourLine(hour, min);
    drawMinLine(min);
    drawSecLine(sec);
    drawCenter();
    ctx.restore();
  }
  function drawCenter() {
    ctx.beginPath();
    ctx.fillStyle = '#ccc';
    ctx.arc(0.0, 0.0, 7.0, 0.0, 2 * math.PI);
    ctx.fill();
  }
  function drawSecLine(sec) {
    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = '#f00';
    ctx.rotate(sec * singleDotsAngle);
    ctx.moveTo(-2.0, 20.0);
    ctx.lineTo(2.0, 20.0);
    ctx.lineTo(1.0, -radius + 18);
    ctx.lineTo(-1.0, -radius + 18);
    ctx.fill();
    ctx.restore();
  }
  function drawMinLine(min) {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 5.0;
    ctx.lineCap = 'round';
    ctx.rotate(min * singleDotsAngle);
    ctx.moveTo(0.0, 12.0);
    ctx.lineTo(0.0, -radius / 2 - 10);
    ctx.stroke();
    ctx.restore();
  }
  function drawHourLine(hour, min) {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 7.0;
    ctx.lineCap = 'round';
    var hourAngle = hour * singleHourAngle;
    var minAngle = min / 60 * singleHourAngle;
    println('minAngle=' + minAngle);
    ctx.rotate(hourAngle + minAngle);
    ctx.moveTo(0.0, 10.0);
    ctx.lineTo(0.0, -radius / 2);
    ctx.stroke();
    ctx.restore();
  }
  var array;
  var singleHourAngle;
  var Math_0 = Math;
  function drawTexts() {
    var $receiver = array;
    var tmp$, tmp$_0;
    var index = 0;
    for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
      var item = $receiver[tmp$];
      var index_0 = (tmp$_0 = index, index = tmp$_0 + 1 | 0, tmp$_0);
      ctx.beginPath();
      ctx.font = '20px Arial';
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      var x = index_0 * singleHourAngle;
      var x_0 = Math_0.cos(x) * (radius - 25);
      var x_1 = index_0 * singleHourAngle;
      var y = Math_0.sin(x_1) * (radius - 25);
      ctx.fillText(array[index_0], x_0, y);
    }
  }
  var dotsRange;
  var singleDotsAngle;
  function drawDots() {
    var tmp$;
    tmp$ = dotsRange.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      ctx.beginPath();
      if (element % 5 === 0) {
        ctx.fillStyle = '#000';
      }
       else {
        ctx.fillStyle = '#ccc';
      }
      var x = element * singleDotsAngle;
      var x_0 = Math_0.cos(x) * (radius - 10);
      var x_1 = element * singleDotsAngle;
      var y = Math_0.sin(x_1) * (radius - 10);
      ctx.arc(x_0, y, 2.0, 0.0, 2 * math.PI);
      ctx.fill();
    }
  }
  function drawBorder() {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 5.0;
    ctx.translate(width / 2, height / 2);
    ctx.arc(0.0, 0.0, radius, 0.0, 2 * math.PI);
    ctx.stroke();
  }
  function drawText() {
    ctx.beginPath();
    ctx.fillText('kotlin\u65F6\u949F', 400.0, 400.0);
  }
  function drawArc() {
    ctx.beginPath();
    ctx.fillStyle = '#f00';
    ctx.arc(100.0, 100.0, 100.0, 0.0, 0.5 * math.PI, true);
    ctx.fill();
  }
  function drawTraigle() {
    ctx.beginPath();
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 3.0;
    ctx.moveTo(200.0, 200.0);
    ctx.lineTo(300.0, 300.0);
    ctx.lineTo(300.0, 200.0);
    ctx.lineTo(200.0, 200.0);
    ctx.fill();
  }
  function drawLine() {
    ctx.beginPath();
    ctx.strokeStyle = '#f00';
    ctx.lineWidth = 10.0;
    ctx.moveTo(0.0, 0.0);
    ctx.lineTo(100.0, 100.0);
    ctx.stroke();
  }
  Object.defineProperty(_, 'canvas', {
    get: function () {
      return canvas;
    }
  });
  Object.defineProperty(_, 'ctx', {
    get: function () {
      return ctx;
    }
  });
  Object.defineProperty(_, 'width', {
    get: function () {
      return width;
    }
  });
  Object.defineProperty(_, 'height', {
    get: function () {
      return height;
    }
  });
  Object.defineProperty(_, 'radius', {
    get: function () {
      return radius;
    }
  });
  _.main_kand9s$ = main;
  _.drawClock = drawClock;
  _.drawCenter = drawCenter;
  _.drawSecLine_za3lpa$ = drawSecLine;
  _.drawMinLine_za3lpa$ = drawMinLine;
  _.drawHourLine_vux9f0$ = drawHourLine;
  Object.defineProperty(_, 'array', {
    get: function () {
      return array;
    }
  });
  Object.defineProperty(_, 'singleHourAngle', {
    get: function () {
      return singleHourAngle;
    }
  });
  _.drawTexts = drawTexts;
  Object.defineProperty(_, 'dotsRange', {
    get: function () {
      return dotsRange;
    }
  });
  Object.defineProperty(_, 'singleDotsAngle', {
    get: function () {
      return singleDotsAngle;
    }
  });
  _.drawDots = drawDots;
  _.drawBorder = drawBorder;
  _.drawText = drawText;
  _.drawArc = drawArc;
  _.drawTraigle = drawTraigle;
  _.drawLine = drawLine;
  var tmp$, tmp$_0;
  canvas = Kotlin.isType(tmp$ = document.getElementById('canvas'), HTMLCanvasElement) ? tmp$ : throwCCE();
  ctx = Kotlin.isType(tmp$_0 = canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$_0 : throwCCE();
  width = window.innerWidth;
  height = window.innerHeight;
  radius = 100.0;
  array = ['3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '1', '2'];
  singleHourAngle = 2 * math.PI / 12;
  dotsRange = new IntRange(0, 59);
  singleDotsAngle = 2 * math.PI / 60;
  main([]);
  Kotlin.defineModule('app', _);
  return _;
}(typeof app === 'undefined' ? {} : app, kotlin);
