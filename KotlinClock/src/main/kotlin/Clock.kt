import org.w3c.dom.*
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Date
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

//获取canvas
val canvas = document.getElementById("canvas") as HTMLCanvasElement
//代码中不能直接操作canvas  需要通过context
val ctx = canvas.getContext("2d") as CanvasRenderingContext2D
//宽度和高度
val width = window.innerWidth
val height = window.innerHeight

val radius = 100.0
fun main(args: Array<String>) {
    //动态的获取宽度和高度
    ctx.canvas.width = width
    ctx.canvas.height = height

//    //绘制一条线
//    drawLine()
//    //绘制三角形
//    drawTraigle()
//    //绘制圆环
//    drawArc()
//    //绘制文本
//    drawText()

    //绘制时钟
    drawClock()
    //定时获取时间  修改时钟  函数包裹{}
    window.setInterval({drawClock()},1000)//定义1秒钟绘制一次
}

fun drawClock() {
    //清空画布
    ctx.clearRect(0.0,0.0,width.toDouble(),height.toDouble())
    //获取当前时间
    val date = Date().asDynamic()//关闭类型检查
    val hour = date.getHours()
    val min = date.getMinutes()
    val sec = date.getSeconds()
    println("hour=$hour min=$min sec=$sec")

    //绘制钟表圆环
    drawBorder()
    //绘制60个点
    drawDots()
    //绘制时间文本
    drawTexts()
    //绘制时针
    drawHourLine(hour,min)
    //绘制分针
    drawMinLine(min)
    //绘制秒针
    drawSecLine(sec)
    //绘制圆心
    drawCenter()

    //恢复画布
    ctx.restore()
}

fun drawCenter() {
    ctx.beginPath()
    ctx.fillStyle="#ccc"
    ctx.arc(0.0,0.0,7.0,0.0,2* PI)
    ctx.fill()
}

fun drawSecLine(sec:Int) {
    ctx.save()
    ctx.beginPath()
    ctx.fillStyle="#f00"
    ctx.rotate(sec*singleDotsAngle)

//    ctx.moveTo(0.0,14.0)
//    ctx.lineTo(0.0,-radius/2-30)
    ctx.moveTo(-2.0, 20.0)
    ctx.lineTo(2.0, 20.0)
    ctx.lineTo(1.0, -radius + 18)
    ctx.lineTo(-1.0, -radius + 18)

//    ctx.stroke()
    ctx.fill()
    ctx.restore()
}

fun drawMinLine(min:Int) {
    ctx.save()
    ctx.beginPath()
    ctx.lineWidth=5.0
    ctx.lineCap=CanvasLineCap.ROUND
    //旋转画布
    ctx.rotate(min*singleDotsAngle)

    ctx.moveTo(0.0,12.0)
    ctx.lineTo(0.0,-radius/2-10)
    ctx.stroke()
    ctx.restore()
}

fun drawHourLine(hour:Int,min: Int) {
    //保存画布
    ctx.save()
    ctx.beginPath()
    ctx.lineWidth=7.0
    //指定末端的样式
    ctx.lineCap=CanvasLineCap.ROUND
    val hourAngle = hour*singleHourAngle
    val minAngle = min/60.toDouble()*singleHourAngle
    println("minAngle=$minAngle")
    //旋转画布
    ctx.rotate(hourAngle+minAngle)

    ctx.moveTo(0.0,10.0)
    ctx.lineTo(0.0,-radius/2)
    ctx.stroke()
    //恢复画布原来状态
    ctx.restore()
}

//定义时间文本数组
val array = arrayOf("3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "1", "2")
val singleHourAngle = 2* PI/12
fun drawTexts() {
    array.forEachIndexed { index, s ->
        ctx.beginPath()
        ctx.font = "20px Arial"
        //水平方向和竖直方向居中
        ctx.textAlign = CanvasTextAlign.CENTER
        ctx.textBaseline = CanvasTextBaseline.MIDDLE

        val x = cos(index * singleHourAngle) * (radius - 25)
        val y = sin(index * singleHourAngle) * (radius - 25)
        ctx.fillText(array[index],x, y)
    }
}

val dotsRange = 0..59
val singleDotsAngle = 2 * PI / 60
fun drawDots() {
    dotsRange.forEach {
        ctx.beginPath()
        if (it % 5 == 0) {
            ctx.fillStyle = "#000"
        } else {
            ctx.fillStyle = "#ccc"
        }
        //求每一个点的x和y
        val x = cos(it * singleDotsAngle) * (radius - 10)
        val y = sin(it * singleDotsAngle) * (radius - 10)
        ctx.arc(x, y, 2.0, 0.0, 2 * PI)
        ctx.fill()
    }
}

fun drawBorder() {
    ctx.save()
    ctx.beginPath()
    ctx.lineWidth = 5.0
    //将画布中心点指定到中心点
    ctx.translate(width / 2.toDouble(), height / 2.toDouble())
    ctx.arc(0.0, 0.0, radius, 0.0, 2 * PI)
    ctx.stroke()
}


/*---------------------------- 绘制基本要素 ----------------------------*/
fun drawText() {
    ctx.beginPath()
    ctx.fillText("kotlin时钟", 400.0, 400.0)
}

fun drawArc() {
    ctx.beginPath()
    //指定填充颜色
    ctx.fillStyle = "#f00"
    ctx.arc(100.0, 100.0, 100.0, 0.0, 0.5 * PI, true)
//    ctx.stroke()
    ctx.fill()
}

fun drawTraigle() {
    //开启一条新路劲
    ctx.beginPath()
    ctx.strokeStyle = "#000"
    ctx.lineWidth = 3.0
    ctx.moveTo(200.0, 200.0)
    ctx.lineTo(300.0, 300.0)
    ctx.lineTo(300.0, 200.0)
    ctx.lineTo(200.0, 200.0)
//    ctx.stroke()
    ctx.fill()
}


fun drawLine() {
    //开启一条新的路劲
    ctx.beginPath()
    //指定颜色
    ctx.strokeStyle = "#f00"
    //指定线条宽度
    ctx.lineWidth = 10.0
    ctx.moveTo(0.0, 0.0)
    ctx.lineTo(100.0, 100.0)
    ctx.stroke()
}
